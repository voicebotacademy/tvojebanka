const admin = require('firebase-admin');
const express = require('express');
const cors = require('cors');
const app = express();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));

var serviceAccount = require("./tvojebanka-firebase-adminsdk.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://tvojebanka.firebaseio.com"
});

function readBalance(id) {
  return admin.database().ref(id + "/balance").once("value");
}

function readTransactions(id) {
  return admin.database().ref(id + "/transactions").once("value");
}

app.get('/:id', (req, res) => {
const date = new Date().toDateString();
const id = req.params.id;
Promise.all([readBalance(id), readTransactions(id)]).then(values => {
  const balance = values[0].val();
  const transactions = values[1].val();

  var transactionsString = "";
  for (var key in transactions) {
    const t = transactions[key];
    transactionsString += `<tr>
      <td>${t.date}</td>
      <td>${t.type === "deposit" ?
      '<i style="color:green" class="fas fa-arrow-right fa-lg"></i>'
      : '<i style="color:red" class="fas fa-arrow-left fa-lg"></i>'
      }</td>
      <td>${t.fromTo}</td>
      <td>${t.amount} Kč</td>
    </tr>`;
  }

  res.status(200).send(`<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>TVOJE BANKA</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  </head>
  <body>
    <h1>TVOJE BANKA</h1>
    <p>
      Stav účtu ke dni ${date} je ${balance} Kč.
    </p>
    <table style="width:100%" class="table">
    ${transactionsString}
    </table>
  </body>
  </html>`);
  return;
}).catch(error => {
  console.error(error);
});
});
app.post('/api/transaction/:id', (req, res) => {
  const data = req.body;
  admin.database().ref(req.params.id + "/transactions").push(data);
  admin.database().ref(req.params.id + "/balance").once("value", value => {
    admin.database().ref(req.params.id + "/balance").set(Number(value.val()) + Number(data.amount));
  });
  res.status(200).end();
});
app.get('/api/transactions/:id', (req, res) => {
  const data = req.body;
  admin.database().ref(req.params.id + "/transactions").once("value", value => {
    res.status(200).json(value.val()).send();
  });
});
app.get('/api/balance/:id', (req, res) => {
  admin.database().ref(req.params.id + "/balance").once("value", value => {
    res.status(200).json({"balance": Number(value.val())}).send();
  });
});

module.exports = app;
