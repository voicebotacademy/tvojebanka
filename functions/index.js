const functions = require('firebase-functions');
const banka = require('./bank.js');
const dialogflowFirebaseFulfillment = require('./dialogflow.js');

// Expose Express API as a single Cloud Function:
exports.banka = functions.https.onRequest(banka);
exports.dialogflowFirebaseFulfillment = functions.https.onRequest(dialogflowFirebaseFulfillment);
