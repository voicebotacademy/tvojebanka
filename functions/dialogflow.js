const { SimpleResponse, SignIn, dialogflow } = require("actions-on-google");
const axios = require('axios');
var clientid = require("./clientid.json");

// Instantiate the Dialogflow client.
const dialogflowApp = dialogflow({
  debug: true,
  verification: {
    Authorization: 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
  },
  clientId: clientid
});

dialogflowApp.intent('Default Welcome Intent', (conv) => {
  const {payload} = conv.user.profile;
  const name = payload ? ` ${payload.given_name}` : '';
  conv.ask(`Hi${name}!\n`);
  conv.ask(`Welcome to the Talking Bank. With this app you can send money to your family members, get your total spendings and account balance.`);
});

dialogflowApp.intent('Get Signin', (conv, params, signin) => {
  if (signin.status === 'OK') {
    const payload = conv.user.profile.payload
    conv.ask(`I got your account details, ${payload.name}. What do you want to do next?`)
  } else {
    conv.ask(`I am sorry but without an account my powers are very limited, but what do you want to do next?`)
  }
})

dialogflowApp.intent('Transaction intent', (conv) => {
  if (!conv.user.email) {
    conv.ask(new SignIn('To get your account details'))
  } else {
    console.log(conv.parameters);
    const amount = conv.parameters.amount;
    const recipient = conv.parameters.recipient;

    return axios({
        url: `https://us-central1-tvojebanka.cloudfunctions.net/banka/api/transaction/${conv.user.id}`,
        method: "POST",
        data: {
          amount: -amount,
          fromTo: recipient,
          date: new Date(),
          type: "withdrawal"
        },
        headers: {
          'content-type': "application/json"
        }
    }).then(result => {
      conv.ask(`Successfuly transfered your money.`);
      return;
    })
    .catch(err => {
      console.error(err);
      conv.ask(`Unable to send your money. ${err}`);
    });
  }
});

dialogflowApp.intent('Balance intent', (conv) => {
  if (!conv.user.email) {
    conv.ask(new SignIn('To get your account details'))
  } else {
    return axios({
        url: `https://us-central1-tvojebanka.cloudfunctions.net/banka/api/balance/${conv.user.id}`,
        method: "GET",
        headers: {
          'accept': "application/json"
        }
    }).then(result => {
      conv.ask(`Your current balance is ${result.data.balance} crowns.`);
      return;
    })
    .catch(err => {
      console.error(err);
      conv.ask(`Unable to get your balance information. ${err}`);
    });
  }
});

dialogflowApp.intent('Total spending intent', (conv) => {
  if (!conv.user.email) {
    conv.ask(new SignIn('To get your account details'))
  } else {
    return axios({
        url: `https://us-central1-tvojebanka.cloudfunctions.net/banka/api/transactions/${conv.user.id}`,
        method: "GET",
        headers: {
          'accept': "application/json"
        }
    }).then(result => {
      var amount = 0;
      for (key in result.data) {
        amount += -result.data[key].amount;
      }

      conv.ask(`Your total spendings are ${amount} crowns.`);
      return;
    })
    .catch(err => {
      console.error(err);
      conv.ask(`Unable to get your total savings. ${err}`);
    });
  }
});

module.exports = dialogflowApp;
