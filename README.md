# Tvojebanka
Repository for sample application demonstrating Google Assistant usage for account management.
Consist of sample bank (web page + simple api) and DialogFlow fullfillment function.

### Prerequisities
To deploy properly to firebase you need to include service account json named as tvojebanka-firebase-adminsdk.json in function folder. Export it from within Firebase console.

Also after setting up account linking in Dialogflow console put client id in clientid.json within function folder.
